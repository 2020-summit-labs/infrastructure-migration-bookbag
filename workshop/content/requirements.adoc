:markup-in-source: verbatim,attributes,quotes
:USER_GUID: %guid%

=== Base requirements

* A computer with access to the Internet :-)
* SSH client (for Microsoft Windows users, link:https://www.putty.org/[Putty] application or built-in bash in Windows 10, are both good choices)
+
Alternatively you can link:javascript:window.open('/terminal',target='_blank');void(0)[click here to open] a web ssh terminal
* Firefox 17 or higher, or Chromium / Chrome
* To fully experience this lab, it is necessary to set up proxy for the duration. 
- That said, you _can_ do the lab _without a proxy_, but you will not be able to access some parts of the environment. This could affect your lab experience somewhat. 

==== Proxy and FoxyProxy plugin setup

To make the environment unique and distinguishable between attendees, a 5 character identifier is assigned to it (i.e. `r1e37`). This identifier will be referred to, within the documentation as *{USER_GUID}*. + 
You can set up a proxy by pointing it in your browser to `workstation-*{USER_GUID}*.rhpds.opentlc.com`, port 3128. Make sure that the proxy type is *HTTP*

Using FoxyProxy is described in more details below.
When setting the proxy up, the proxy server should be used for all protocols in order to access the environment.

If you are using FoxyProxy here are the relevant settings:

[cols="1,2",options="header"]
|=======
| Option | Value
| Proxy IP address or DNS name: | `workstation-*{USER_GUID}*.rhpds.opentlc.com`
|Port|3128
| Type| HTTP |
|=======
*Foxy Proxy White Pattern list:*
[cols="1,1,1",options="header"]
|=======
|Name|Pattern|Type
|example.com|*.example.com|Wildcard
|192.168|192.168.0.*|Wildcard
|=======

When you configure FoxyProxy this way, only the patterns pertaining to the lab will be going through the proxy, the rest of your internet requests will use your default internet access.

[NOTE]
Grammarly plugin for Chrome may cause problems when managing CloudForms. If you have it in your browser, please deactivate it while doing this lab.