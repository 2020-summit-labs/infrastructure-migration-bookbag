:markup-in-source: verbatim,attributes,quotes
:imagesdir: images 
:USER_GUID: %guid%

== Creating a Migration Plan

Now that we have the Infrastructure mapped for both source and the destination clusters, we can get to the core of the matter. 
Creating a Migration Plan will enable us to choose which VMs we would like to migrate. There can be many plans created depending on internal or logical system separation. 
Maybe some systems cannot be migrated before a pre-requisite systems have been moved as well. 
The choice and planning of the migration is at the system administrator discretion. + 
So let's get on with it!


. Start in the CloudForms page accessed by navigating to *Migration -> Migration Plans*.
+
image::migration_plan_0.png[Migration Plan 0]

. Click on *Create Migration Plan*.

. In the *step 1* of the wizard, *General*, select in the drop down menu the *Infrastructure Mapping* to be used, `VMware to RHV`, add the name `Summit 2020 Lab` and click *Next*.
+
image::migration_plan_1.png[Migration Plan 2]
+
[NOTE]
Keeping the default option for *Select VMs* will take us to the VM menu selector. In this step we will be migrating the `hana`, `oracledb` and `tomcat` virtual machines.
For massive conversions, there is an option to use a CSV file upload, which is a better option in those cases.

. In the *step 2* of the wizard, *VMs*, we will choose the 3 VMs to be migrated to RHV. 
Please select, *oracledb*, *hana* and *tomcat* virtual machines to be migrated.
+
image::migration_plan_2.png[Migration Plan 3]

. In the *step 3* of the wizard, *Advanced Options*, we can assign *Pre* and *Post* migration playbooks to be executed during the migration. 
Since we have a couple of servers which are running distributions we would like to convert to RHEL during the conversion, we will enable *post* playbooks for them. 
Click on a *Select postmigration playbook service* drop-down and choose `PostMigration - Convert2RHEL`
+
image::migration_plan_3.png[Migration Plan 4]
+
In the same step make sure we select the VMs that need to be converted. Those are `oracledb` and `tomcat` (currently running Oracle Linux and CentOS, respectively).

. In the *step 4* of the wizard, *Schedule*, select *Start migration immediately* and click *Create*.
The wizard will close and the migration of the VMs will start immediately. 
+
image::migration_plan_4.png[Migration Plan 4]
+
[NOTE]
The migration plan can be scheduled to be run at a later time, by choosing the other option.

. In the *step 5* of the wizard, *Results*, the message `Migration Plan: 'Summit 2020 Lab' is in progress` will be displayed. Click *Close*.
+
image::migration_plan_5.png[Migration Plan 5]

=== Monitor the Migration

. After you click the close button, you are presented with a page showing the migration plans In Progress. It may take up to a minute for the progress box to start updating.
+
image::migration_running_1.png[Migration Running 3]

. Now the migration is executing. It takes some time for the pre-migration steps to be finished and the conversion process to start.
If we wished to, we could see the orchestration process in CloudForms logs.
From the workstation terminal you can SSH into CloudForms and tail the logs at on `cf.example.com` in the following directory: `/var/www/miq/vmdb/log/automation.log`. 
+
You can tail this file with `tail -f /var/www/miq/vmdb/log/automation.log`.
+
Word of caution: the `automation.log` is storing a lot of logs and can present a huge amount of scrolling text. 
+
[source,bash,subs="{markup-in-source}"]
----
[root@workstation ~]# ssh cf.example.com
[root@cf ~]# tail -f /var/www/miq/vmdb/log/automation.log
----
+
Once the pre-migration steps are finished and the conversion starts, each VM conversion process can be tracked in the Conversion Host.
Our conversion hosts are kvm1 and kvm2. So, for example we could do:
+
[source,bash,subs="{markup-in-source}"]
----
[root@workstation ~]# ssh kvm1.example.com
[root@kvm1 ~]# tail -f /var/log/vdsm/import/v2v-import-*.log
----

. CloudForms Migration interface shows migration status too.
Clicking on the running plan info box with the name `Summit 2020 Lab` will display the detailed info of the status. The screenshot below shows migration progress after 
some amount of time and might differ from what you currently see on the screen:
+
image::migration_running_2.png[Migration Running 2]

. As far as migration goes, `hana` is the only one which will not need to be converted. This means that it will get migrated and be running in the destination as first.
The other two machines, `tomcat` and `oracledb` will need more time, because after converting the disk to RHV, the OS still needs some time for the conversion to RHEL. 
The longest migration time is needed by the most complex VM deployment, and that is OracleDB running on Oracle Linux. + 
+
The total migration time is approximately `55-60` minutes, depending on the load on the cloud servers supporting the environment at the moment.
A thing to note is that the VMs being migrated are powered off during the migration process. + 
This can be seen if you navigate to *Compute -> Infrastructure -> Virtual Machines* in CloudForms. You may see double entries in the CloudForms UI, but those are both migrated and source machines presented on the different providers. They can be differentiated by the icon in the lower left corner of each VM.
+
image::migration_running_3.png[Migration Running 3]
+
You can return to the Migration Progress by clicking *Migration -> Migration Plans*. Click on the running plan to get back to the details.
. After about `30` minutes, all of the VMs have finished with migration and those that need to be converted are running playbooks which will execute the conversion to RHEL. 
At this point it should already be visible in the list of VMs in RHV Web UI, as shown in the previous screenshot. 
[NOTE]
The following step can be only be accessed with the proxy enabled in the browser
+
Switch to RHV Manager tab in your browser and click on *Compute -> Virtual Machines*.  All the VMs will be now visible in the web UI of Red Hat Virtualization as being powered up or already running. If by any chance some of the VMs are shown as down, and the playbook is running, it may be in the process of creating a snapshot or rebooting during the conversion. 
+
image::migration_running_rhv.png[Migration Running 9]
. Go back to the CloudForms tab. It takes additional `15-20` minutes in this environment for the playbooks to complete on the VMs being converted. 
CloudForms is now showing us that the migration has been completed successfully. 
The final view of the Migration Page should look something like this:
+
image::migration_running_finish.png[Migration Finished]

. Let's check if the VMs are up and running. Go to workstation VM and execute the following command:
+
[source,bash,subs="{markup-in-source}"]
----
[root@workstation-repl ~]# ansible apps -m ping -o

oracledb.example.com | SUCCESS => {"ansible_facts": {"discovered_interpreter_python": "/usr/bin/python"}, "changed": false, "ping": "pong"}
hana.example.com | SUCCESS => {"ansible_facts": {"discovered_interpreter_python": "/usr/bin/python"}, "changed": false, "ping": "pong"}
tomcat.example.com | SUCCESS => {"ansible_facts": {"discovered_interpreter_python": "/usr/bin/python"}, "changed": false, "ping": "pong"}

[root@workstation-repl ~]#
----

. We can see that all of the VMs are operational and accessible. We can do a migration review within CloudForms as well
If you go to the main Migration Plans page of CloudForms you should see something similar to the following screenshot:
+
image::migration_finished_1.png[Migration Finished 1]
+
Here you can see that the migration has completed successfully, along with the details about Infrastructure Mapping used and total time for the migration.

. Additionally the migration logs can be downloaded and accessed post VM migration. This is useful for troubleshooting errors or just to check the migration details. It's worth mentioning that if the migration fails prior to the start of VM disk conversion, this log will not be available. The logs are in plain text format and can be quite large, so the download can take some time. +
To access the log just click on the plan we just migrated, choose a VM from the list and click **Download Log**. From the drop-down you can choose which log you would like to download.
+
image::migration_log.png[Migration Log Access]